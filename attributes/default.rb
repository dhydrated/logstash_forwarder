default.logstash_forwarder.version = '0.3.1'
default.logstash_forwarder.config_file = '/etc/logstash-forwarder'
default.logstash_forwarder.user = 'root'
default.logstash_forwarder.group = 'root'
default.logstash_forwarder.crt_file = '/etc/pki/tls/certs/logstash.crt'
default.logstash_forwarder.key_file = '/etc/pki/tls/private/logstash.key'
default.logstash_forwarder.timeout = 15
default.logstash_forwarder.paths = ['/var/log']
default.logstash_forwarder.config.network.servers = [ "127.0.0.1:12345" ]
default.logstash_forwarder.config.files = []
default.logstash_forwarder.params = "-spool-size 100 -from-beginning true"

